const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
var port = process.env.PORT || 3030

const { v4: uuidv4 } = require('uuid')
const { ExpressPeerServer } = require('peer')
const peerServer = ExpressPeerServer(server, {
  debug: true
})

app.set('view engine', 'ejs')
app.use(express.static('public'))

app.use('/peerjs', peerServer)
app.get('/', (req, res) => {
  res.redirect(`/${uuidv4()}`)
})

app.get('/:room', (req, res) => {
  res.render('room', { roomId: req.params.roomId })
})

io.on('connection', socket => {
  socket.on('join-room', (roomId, userId) => {
    socket.join(roomId)
    socket.to(roomId).broadcast.emit('user-connected', userId)
  })
})

server.listen(port, () => {
  console.log('listening', port)
})
