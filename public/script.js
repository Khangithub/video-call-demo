const socket = io('/')
const videoGrid = document.getElementById('video-grid')
const myVideo = document.createElement('video')
myVideo.muted = true

var peer = new Peer()

let myVideoStream

navigator.mediaDevices
  .getUserMedia({
    video: true,
    audio: true
  })
  .then(stream => {
    myVideoStream = stream
    addVideoStream(myVideo, stream)

    socket.on('user-connected', userId => {
      connectToNewUser(userId, stream)
    })

    peer.on('call', call => {
      call.answer(stream)
      const video = document.createElement('video')
      call.on('stream', userVideoStream => {
        addVideoStream(video, userVideoStream)
      })
    })
  })

peer.on('open', id => {
  console.log(id)
  socket.emit('join-room', ROOM_ID, id)
})

function connectToNewUser (userId, stream) {
  const call = peer.call(userId, stream)
  const video = document.createElement('video')
  call.on('stream', userVideoStream => {
    console.log(userVideoStream)
    addVideoStream(video, userVideoStream)
  })
}

function addVideoStream (video, stream) {
  video.srcObject = stream
  video.addEventListener('loadedmetadata', () => {
    video.play()
  })
  videoGrid.append(video)
}
